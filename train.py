#!/usr/bin/env python

"""
"""

import re
import numpy as np
import pandas as pd
import tensorflow as tf

import ujson as json

import shutil

from sklearn.preprocessing import LabelBinarizer
from sklearn.utils.class_weight import compute_class_weight

from keras import backend as K
from keras.models import Model
from keras.layers import Dense, Input, Embedding
from keras.layers import LSTM, Bidirectional, Dropout
from keras.layers.merge import concatenate
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.core import Flatten, Reshape
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s : %(levelname)s : %(message)s')
logger = logging.getLogger('hadr_clf')

CORES = 4

SEED = 42
np.random.seed(SEED)


with open("data/stopwords.txt", "r") as fi:
    STOPWORDS = set([line.strip() for line in fi.readlines()])


TOKENS = re.compile(r"""
    (?:[a-z][a-z'\-_]+[a-z])       # Words with apostrophes or dashes.
    |
    (?:[+\-]?\d+[,/.:-]\d+[+\-]?)  # Numbers, including fractions, decimals.
    |
    (?:[\w_]+)                     # Words without apostrophes or dashes.
    |
    (?:\.(?:\s*\.){1,})            # Ellipsis dots.
    |
    (?:\S)                         # Everything else that isn't whitespace.
    """, re.VERBOSE | re.I | re.UNICODE)


def tokenize(text):
    text = text.lower()
    tokens = TOKENS.findall(text)
    tokens = [t for t in tokens if t not in STOPWORDS]
    return tokens


def build_Conv2dCNN(timesteps, vocab_dim, n_symbols, embedding_weights,
                    num_filters, filter_sizes, drop, n_classes, lr, **kwargs):
    """
    Model lifted wholesale from:

        https://github.com/bhaveshoswal/CNN-text-classification-keras

    """
    seq_input = Input(shape=(timesteps,), dtype='int32', name="inputLayer")
    embedding = Embedding(output_dim=vocab_dim,
                          input_dim=n_symbols,
                          weights=[embedding_weights],
                          trainable=False)(seq_input)
    reshape = Reshape((timesteps, vocab_dim, 1))(embedding)

    conv_0 = Conv2D(num_filters, (filter_sizes[0], vocab_dim),
                    activation='relu', padding="valid",
                    data_format="channels_last",
                    kernel_initializer="normal")(reshape)
    conv_1 = Conv2D(num_filters, (filter_sizes[1], vocab_dim),
                    activation='relu', padding="valid",
                    data_format="channels_last",
                    kernel_initializer="normal")(reshape)
    conv_2 = Conv2D(num_filters, (filter_sizes[2], vocab_dim),
                    activation='relu', padding="valid",
                    data_format="channels_last",
                    kernel_initializer="normal")(reshape)

    maxpool_0 = MaxPooling2D(pool_size=(timesteps - filter_sizes[0] + 1, 1),
                             strides=(1, 1), padding="valid",
                             data_format="channels_last")(conv_0)
    maxpool_1 = MaxPooling2D(pool_size=(timesteps - filter_sizes[1] + 1, 1),
                             strides=(1, 1), padding="valid",
                             data_format="channels_last")(conv_1)
    maxpool_2 = MaxPooling2D(pool_size=(timesteps - filter_sizes[2] + 1, 1),
                             strides=(1, 1), padding="valid",
                             data_format="channels_last")(conv_2)

    merged_tensor = concatenate([maxpool_0, maxpool_1, maxpool_2])
    flatten = Flatten()(merged_tensor)
    # reshape = Reshape((3*num_filters,))(merged_tensor)
    dropout = Dropout(drop)(flatten)
    output = Dense(n_classes, activation='sigmoid',
                   name="inferenceLayer")(dropout)

    # this creates a model that includes
    model = Model(seq_input, output)
    adam = Adam(lr=lr)
    model.compile(loss='binary_crossentropy', optimizer=adam,
                  metrics=['categorical_accuracy'])
    model.summary()
    return model


def build_LSTM(timesteps, vocab_dim, n_symbols, embedding_weights, lstm_dim,
               lr, drop, n_classes, hidden_layers, **kwargs):
    seq_input = Input(shape=(timesteps,), dtype='int32', name="inputLayer")
    embedding = Embedding(output_dim=vocab_dim,
                          input_dim=n_symbols,
                          weights=[embedding_weights],
                          trainable=False,
                          name="GloVe")(seq_input)
    lstm = Bidirectional(LSTM(lstm_dim, dropout=drop, recurrent_dropout=drop,
                              return_sequences=(hidden_layers > 0)),
                         name="BiLSTM_0")(embedding)
    for i in range(hidden_layers):
        lstm = Bidirectional(LSTM(lstm_dim, dropout=drop,
                                  recurrent_dropout=drop,
                                  return_sequences=(i+1 < hidden_layers)),
                             name="BiLSTM_%d" % (i+1))(lstm)
    output = Dense(n_classes, activation='softmax',
                   name="inferenceLayer")(lstm)
    model = Model(seq_input, output)
    adam = Adam(lr=lr)
    model.compile(loss='categorical_crossentropy', optimizer=adam,
                  metrics=['categorical_accuracy'])
    model.summary()
    return model


if __name__ == "__main__":

    sess = tf.Session()
    K.set_session(sess)

    # # DATA # #

    # Load corpus
    logger.info("Loading texts.")
    train = pd.read_json("data/train.json", lines=True)
    test = pd.read_json("data/test.json", lines=True)

    # Convert tokens to sequences of integers
    logger.info("Preparing sequence data.")

    nb_words = 5000
    timesteps = 25

    train.text = train.text.apply(tokenize)
    test.text = test.text.apply(tokenize)

    tok = Tokenizer(num_words=nb_words, filters='')
    tok.fit_on_texts(train.text.tolist() + test.text.tolist())

    vocab = dict(sorted(tok.word_index.items(),
                        key=lambda x: tok.word_counts[x[0]])[-nb_words:])
    with open("data/dic.json", "w") as fo:
        json.dump(vocab, fo)

    X_train_seq = pad_sequences(tok.texts_to_sequences(train.text),
                                maxlen=timesteps)
    X_test_seq = pad_sequences(tok.texts_to_sequences(test.text),
                               maxlen=timesteps)

    # Load word embeddings
    logger.info("Preparing embedding weights.")

    vocab_dim = 200
    n_symbols = max(vocab.values()) + 1

    glove = np.load("models/glove.6B.200d.npy")
    idx2word = dict(enumerate(
        np.load("/data/GloVe/glove.6B.200d.voc.npy").tolist()))
    word2idx = {v: k for k, v in idx2word.items()}

    embedding_weights = np.zeros((n_symbols, vocab_dim))
    for word, index in vocab.items():
        if word in word2idx:
            embedding_weights[index, :] = glove[word2idx[word]]

    logger.info("OOV: %.2f", (embedding_weights.sum(0) == 0).mean())

    # # MODELING # #

    model_name = "hadr-clf-bilstm"

    # Binarize class labels
    lb = LabelBinarizer()
    y_train = lb.fit_transform(train.topic)
    y_test = lb.fit_transform(test.topic)
    with open("models/%s.classes" % model_name, "w") as fo:
        fo.write("\n".join(lb.classes_))

    # Build
    fit_params = {"batch_size": 64,
                  "epochs": 20,
                  "verbose": 1,
                  "shuffle": True,
                  }

    model_params = {"n_classes": len(lb.classes_),
                    "lstm_dim": 200,
                    "vocab_dim": vocab_dim,
                    "n_symbols": n_symbols,
                    "timesteps": timesteps,
                    "hidden_layers": 2,
                    "lr": 0.0001,
                    "drop": 0.2,
                    "filter_sizes": [3, 4, 5],
                    "num_filters": 512,
                    }

    logger.info("Building.")
    model = build_LSTM(embedding_weights=embedding_weights, **model_params)
#   model = build_Conv2dCNN(embedding_weights=embedding_weights,
#                           **model_params)

    logger.info("Fitting.")
    model_path = "models/weights.h5"

    # Set up callbacks
    checkpoint = ModelCheckpoint(model_path, verbose=0,
                                 monitor='val_loss',
                                 save_best_only=True,
                                 save_weights_only=True,
                                 mode="auto", period=1)
    early_stop = EarlyStopping(monitor='val_loss', patience=3, verbose=1)
    class_weights = np.array([compute_class_weight('balanced',
                                                   np.unique(l), l)
                              for l in y_train.T])

    # Fit model
    model.fit(X_train_seq, y_train, callbacks=[checkpoint, early_stop],
              class_weight=class_weights, validation_data=(X_test_seq, y_test),
              **fit_params)

    model.load_weights(model_path)
    model.classes_ = lb.classes_
    model.save("models/%s.h5" % model_name)

    # Write out the layer names
    for n in sess.graph.as_graph_def().node:
        if "inferenceLayer" in n.name or "inputLayer" in n.name:
            print(n.name)

    # Use TF to save the graph model instead of Keras save model to load it in
    # Golang
    try:
        shutil.rmtree("models/%s" % model_name)
        logger.warning("Deleting old model directory.")
    except Exception:
        logger.warning("Creating new model directory.")
    builder = tf.saved_model.builder \
        .SavedModelBuilder("models/%s" % model_name)
    # Tag the model, required for Go
    builder.add_meta_graph_and_variables(sess, ["hadrClf"])
    builder.save()
    sess.close()

// Handles loading and preprocessing of a corpus for inference.
package main

import (
	"encoding/json"
	"io/ioutil"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	MAXLEN = 25
)

// A Corpus represents all the data to be classified.
type Corpus struct {
	text       string
	tokens     [][]int32
	dictionary map[string]int32
	stopwords  []string
}

// Instantiates a new corpus from (absolute) file paths for:
// * A text file of samples to be classified;
// * A stopword list;
// * A dictionary file mapping tokens to integers.
func NewCorpus(textFile, stopFile, dicFile string) (corpus Corpus) {
	if err := corpus.loadText(textFile); err != nil {
		log.WithFields(log.Fields{
			"file": textFile}).Fatal("Text file not found.")
	}
	if err := corpus.loadStopWords(stopFile); err != nil {
		log.WithFields(log.Fields{
			"file": textFile}).Fatal("Stopword file not found.")
	}
	if err := corpus.loadDictionary(dicFile); err != nil {
		log.WithFields(log.Fields{
			"file": textFile}).Fatal("Dictionary file not found.")
	}
	corpus.tokenize()
	corpus.pad("pre")
	return
}

// Loads a newline separated text file of samples to be classified.
func (c *Corpus) loadText(fn string) (err error) {
	b, err := ioutil.ReadFile(fn)
	c.text = strings.TrimSpace(string(b))
	return
}

// Loads a newline separated stopword list.
func (c *Corpus) loadStopWords(fn string) (err error) {
	b, err := ioutil.ReadFile(fn)
	c.stopwords = strings.Split(strings.TrimSpace(string(b)), "\n")
	return
}

// Loads a JSON dictionary file mapping tokens to integers.
func (c *Corpus) loadDictionary(fn string) (err error) {
	var b []byte
	if b, err = ioutil.ReadFile(fn); err != nil {
		return
	}
	if err = json.Unmarshal(b, &c.dictionary); err != nil {
		return
	}
	for k, v := range c.dictionary {
		c.dictionary[k] = v
	}
	return nil
}

// Regex tokenizer specification.
var reTokens = regexp.MustCompile(
	// Words with apostrophes or dashes.
	`(?:[a-z][a-z'\-_]+[a-z])|` +
		// Numbers, including fractions, decimals.
		`(?:[+\-]?\d+[,/.:-]\d+[+\-]?)|` +
		// Words without apostrophes or dashes.
		`(?:[\w_]+)|` +
		// Ellipsis dots.
		`(?:\.(?:\s*\.){1,})|` +
		// Everything else that isn't whitespace.
		`(?:\S)`,
)

// Separates text at word and numeral boundaries and converts
// to numerals for subsequent processing.
func (c *Corpus) tokenize() {
	for _, line := range strings.Split(c.text, "\n") {
		var tokens []int32
		for _, token := range reTokens.FindAllString(line, -1) {
			if !contains(c.stopwords, token) {
				tokens = append(tokens, c.dictionary[token])
			}
		}
		c.tokens = append(c.tokens, tokens)
	}
}

// Pads short (<MAXLEN) samples with zeros. Also truncates samples to MAXLEN.
func (c *Corpus) pad(padding string) {
	for i, sample := range c.tokens {
		if len(sample) > MAXLEN {
			sample = sample[0:MAXLEN]
		}
		for len(sample) < MAXLEN {
			if padding == "pre" {
				sample = append([]int32{0}, sample...)
			} else {
				sample = append(sample, 0)
			}
		}
		c.tokens[i] = sample
	}
}

// Reverses the order of tokens in a slice of samples.
func reverse(tokens [][]int32) (rev_tokens [][]int32) {
	for i, sample := range tokens {
		for i := len(sample)/2 - 1; i >= 0; i-- {
			o := len(sample) - 1 - i
			sample[i], sample[o] = sample[o], sample[i]
		}
		rev_tokens[i] = sample
	}
	return
}

// Checks for membership of a string in a slice of strings.
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

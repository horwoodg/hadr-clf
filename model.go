// Handles loading and application of a Tensorflow model to a corpus.
package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"
	tf "github.com/tensorflow/tensorflow/tensorflow/go"
)

// A Model represents the neural network and its label space.
type Model struct {
	network *tf.SavedModel
	classes []string
}

// Instantiates a new model from a filename and tag.
func NewModel(modelFile, modelTag string) (model Model) {
	model.load(modelFile, modelTag)
	return
}

// Loads a pretrained neural network, along with the class label strings
// used at inference time.
func (m *Model) load(dirname, tag string) (err error) {
	if m.network, err = tf.LoadSavedModel(dirname, []string{tag}, nil); err != nil {
		log.WithFields(log.Fields{
			"dir": dirname}).Fatal("Model directory not found.")
	}
	var b []byte
	if b, err = ioutil.ReadFile(dirname + ".classes"); err != nil {
		log.WithFields(log.Fields{
			"file": dirname + ".classes"}).Fatal("Classes file not found.")
	}
	m.classes = strings.Split(strings.TrimSpace(string(b)), "\n")
	return
}

// Performs inference over a corpus. Returns the label (string) of the
// highest probability class for each samples.
func (m *Model) predict(corpus Corpus) (predictions []string, err error) {
	matrix := buildTensor(corpus.tokens)
	results, err := m.network.Session.Run(
		map[tf.Output]*tf.Tensor{
			m.network.Graph.Operation("inputLayer").Output(0): matrix,
		},
		[]tf.Output{
			m.network.Graph.Operation("inferenceLayer/Softmax").Output(0),
		},
		nil)
	for _, p := range results[0].Value().([][]float32) {
		idx := argmax(p)
		predictions = append(
			predictions, fmt.Sprintf("%s (%.2f)", model.classes[idx], p[idx]))
	}
	return
}

// Iterates over a slice of floats and returns the index of the largest.
func argmax(args []float32) (idx int) {
	max, idx := args[0], 0
	for i, arg := range args {
		if arg > max {
			idx = i
			max = arg
		}
	}
	return
}

// Converts a 2D slice to a Tensor
func buildTensor(a [][]int32) (matrix *tf.Tensor) {
	if matrix, err = tf.NewTensor(a); err != nil {
		log.Fatal(err)
	}
	return
}

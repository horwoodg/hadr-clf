Comparison of Python and Go for Tensorflow LSTM Inference
=========================================================

This repository contains data and code to compare Tensorflow model inference in Go and Python. A number of blog posts have recently demonstrated the simplicity of training a neural network in keras/Tensorflow and [deploying in Go](https://www.google.com/amp/www.tonytruong.net/running-a-keras-tensorflow-model-in-golang/amp/). Direct comparison of inference performance (i.e., in time) is typically thin or abscent from these discussions, however, and nowhere is there comparison of recurrent neural network inference for a natural language processing task. This repository seeks to fill that gap.

Surprisingly, the Go deployment of a Tensorflow stacked BiLSTM fared slightly worse than the keras implementation in Python. The difference was very slight, however, measurable in single-digit seconds for thousands of samples, and may be outweighed by the performance benefits of the Go constellation of tools for deploying, e.g., web services.

## Data

A collection of sentences and phrases from a disaster relief corpus. The corpus consists of 45000 training samples and 6723 testing, examples of which are shown below. Note that labels are taken from crowd-sourced groupings of the phrases and sentences, rather than true topic annotations, and thus are quite noisy (!). 

    {"topic":"extreme violence, terrorism","text":"terrorist act"}
    {"topic":"elections and politics","text":"cynical opportunism"}
    {"topic":"volunteer or professional services","text":"The Red Cross plans to reallocate disaster relief funds to North Korea to assist with cleanup and rebuilding efforts from recent floods that devastated a special economic zone in the northeastern part of the country, sources familiar with the situation said."}
    {"topic":"food","text":"drip irrigation"}
    {"topic":"water","text":"streams creeks"}
    {"topic":"extreme violence, terrorism","text":"guerrilla campaign"}
    {"topic":"flood","text":"Flash floods cause the highest number of casualties; coastal and slow-onset floods have not caused major emergencies so far."}
    {"topic":"landslide","text":"heavy rain floods"}
    {"topic":"sanitation","text":"environmental degradation"}
    {"topic":"infrastructure and utilities","text":"The cluster's procurement pipeline which includes more than 114,500 tents and 521,500 plastic sheets, will assist a further 375,500 families in coming days."}

## Model

A multiclass, multilabel classifier was trained as a stacked BiLSTM using keras/Tensorflow with largely default hyperparameters (20% dropout on LSTM weights; embeddings fixed from GloVe; softmax decision layer). Below is the output from keras's model.summary():

    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inputLayer (InputLayer)      (None, 25)                0         
    _________________________________________________________________
    GloVe (Embedding)            (None, 25, 200)           1004000   
    _________________________________________________________________
    BiLSTM_0 (Bidirectional)     (None, 25, 400)           641600    
    _________________________________________________________________
    BiLSTM_1 (Bidirectional)     (None, 25, 400)           961600    
    _________________________________________________________________
    BiLSTM_2 (Bidirectional)     (None, 400)               961600    
    _________________________________________________________________
    inferenceLayer (Dense)       (None, 25)                10025     
    =================================================================
    Total params: 3,578,825
    Trainable params: 2,574,825
    Non-trainable params: 1,004,000
    _________________________________________________________________


The performance of the model on the test partition of the corpus is not stellar, but given the considerable label confusion in the source data, this is understandable.

                                        precision    recall  f1-score   support
                             cold wave       0.74      0.48      0.59       200
                               drought       0.59      0.28      0.38       140
                            earthquake       0.62      0.51      0.56       169
                elections and politics       0.84      0.67      0.74       418
                                energy       0.77      0.61      0.68       326
                            evacuation       0.59      0.27      0.37       306
           extreme violence, terrorism       0.72      0.45      0.56       399
                                 flood       0.44      0.29      0.35       190
                                  food       0.85      0.70      0.77       411
                              heatwave       0.63      0.20      0.30        96
          infrastructure and utilities       0.57      0.22      0.32       390
                    insect infestation       0.86      0.32      0.47       119
                          intervention       0.60      0.34      0.43       336
                             landslide       0.52      0.09      0.16       142
                               medical       0.84      0.82      0.83       425
                                 money       0.77      0.59      0.67       202
                            sanitation       0.58      0.16      0.25       342
                     search and rescue       0.63      0.28      0.39       295
                               shelter       0.73      0.48      0.58       372
                      tropical cyclone       0.60      0.38      0.46       224
                               tsunami       0.52      0.13      0.21       178
                  violent civil unrest       0.68      0.49      0.57       371
    volunteer or professional services       0.72      0.41      0.53       164
                                 water       0.54      0.30      0.38       398
                              wildfire       0.65      0.45      0.53       110

                           avg / total       0.68      0.43      0.51      6723

The model was trained with keras in Python (see `train.py`) and saved off to a Tensorflow graph for subsequent loading into Go.

## Time Comparison

Independent inference applications were created for each language (i.e., `infer.{go,py}`) and mirror one another in structure to the extent possible. Each reads samples, a stopword list, and a dictionary file from disk; tokenizes samples according to the same regex patterns; pads and truncates samples to integer sequences for modeling; makes predictions with a loaded Tensorflow model; and returns the maximum likelihood label for each sample, along with a posterior probability. The Go package was compiled to a binary. Testing was performed on a quad-core i7 processor.

The figures below indicate the performance of each implementation on the test set. While Go is overall faster, the time differential comes largely from loading the Tensorflow model. The classification time for Python is shorter than that of Go, meaning that the ~7s improvement of Go over Python would in fact disappear in a stateful application that loaded the model once and listened for independent inference calls subsequently.

### Python

    $ time ./infer.py data/test.txt > res0
    Model Load time: 8.240s
    Corpus Load time: 0.097s
    Classification time: 11.854s

    real    0m21.747s
    user    1m17.312s
    sys     0m7.864s


### Go

    $ time ./hadrclf data/test.txt > res1
    Model Load Time: 0.509s
    Corpus Load Time: 0.073s
    Classification Time: 13.412s

    real    0m14.041s
    user    1m31.664s
    sys     0m5.800s

The problem appears to scale linearly with larger sample sizes. When the implementations are run over the entire training set of 45K samples, classification time for Python is 82s, where Go is 98s.

## Output Comparison

Differences are also found in the outputs of the two implementations, despite identical models and preprocessing steps. Alarmingly, this resulted in completely different labels in ~6% of samples, examples of which are shown below.

    $ sdiff -bBWs res0 res1 | head -20
    flood (0.41)                              | flood (0.47)
    sanitation (0.59)                         | sanitation (0.52)
    extreme violence, terrorism (0.73)        | extreme violence, terrorism (0.74)
    drought (0.68)                            | drought (0.63)
    water (0.50)                              | water (0.49)
    cold wave (0.82)                          | cold wave (0.84)
    food (0.28)                               | food (0.33)
    shelter (0.53)                            | shelter (0.56)
    infrastructure and utilities (0.43)       | infrastructure and utilities (0.45)
    extreme violence, terrorism (0.66)        | extreme violence, terrorism (0.56)
    tropical cyclone (0.53)                   | tsunami (0.27)                         <=== different labels
    shelter (0.72)                            | shelter (0.68)
    money (0.42)                              | money (0.52)
    flood (0.50)                              | flood (0.48)
    volunteer or professional services (0.47) | volunteer or professional services (0.50)
    medical (0.43)                            | intervention (0.33)                    <=== different labels
    infrastructure and utilities (0.22)       | infrastructure and utilities (0.24)
    infrastructure and utilities (0.22)       | infrastructure and utilities (0.24)


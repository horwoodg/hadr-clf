#!/usr/bin/env python

"""
Loads a pre-trained Tensorflow model, loads and preprocesses a test corpus,
performs inference, and returns a set of labels.
"""

import time
import re
import sys
import pandas as pd

import ujson as json

from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.models import load_model

import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s : %(levelname)s : %(message)s')
logger = logging.getLogger('hadr_clf')


with open("data/stopwords.txt", "r") as fi:
    STOPWORDS = set([line.strip() for line in fi.readlines()])


TOKENS = re.compile(r"""
    (?:[a-z][a-z'\-_]+[a-z])       # Words with apostrophes or dashes.
    |
    (?:[+\-]?\d+[,/.:-]\d+[+\-]?)  # Numbers, including fractions, decimals.
    |
    (?:[\w_]+)                     # Words without apostrophes or dashes.
    |
    (?:\.(?:\s*\.){1,})            # Ellipsis dots.
    |
    (?:\S)                         # Everything else that isn't whitespace.
    """, re.VERBOSE | re.I | re.UNICODE)


def tokenize(text):
    text = text.lower()
    tokens = TOKENS.findall(text)
    tokens = [t for t in tokens if t not in STOPWORDS]
    return tokens


if __name__ == "__main__":

    # Load model.
    model_name = "hadr-clf-bilstm"
    t = time.time()
    model = load_model("models/%s.h5" % model_name)
    sys.stderr.write("Model Load time: %.3fs\n" % (time.time() - t))

    # Load data.
    t = time.time()
    with open(sys.argv[1], "r") as fi:
        data = [line.strip() for line in fi.readlines()]
        test = pd.DataFrame({"text": data})

    # Preprocess data
    nb_words = 5000
    timesteps = 25

    test.text = test.text.apply(tokenize)

    tok = Tokenizer(num_words=nb_words, filters='')
    with open("data/dic.json", "r") as fi:
        vocab = json.load(fi)
    tok.word_index = vocab

    X_test_seq = pad_sequences(tok.texts_to_sequences(test.text),
                               maxlen=timesteps)
    sys.stderr.write("Corpus Load time: %.3fs\n" % (time.time() - t))

    with open("models/%s.classes" % model_name, "r") as fi:
        classes = [line.strip() for line in fi.readlines()]
    
    # Perform inference
    t = time.time()

    X_test_pred = model.predict(X_test_seq)

    results = ["%s (%.2f)" % (classes[v], X_test_pred[i, v])
               for i, v in enumerate(X_test_pred.argmax(1))]

    for r in results:
        print(r)

    sys.stderr.write("Classification time: %.3fs\n" % (time.time() - t))

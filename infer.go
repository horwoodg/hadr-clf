// Package main implements a command line utility for inferring disaster relief
// labels from text. Report labels to stdout and completion times to stderr.
package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"

	docopt "github.com/docopt/docopt-go"
	log "github.com/sirupsen/logrus"
)

var (
	err   error
	model Model
)

var usage = `HA/DR Classifier

Usage: hadrclf [options] DATAFILE

Arguments:
  DATAFILE	Newline-delimited text file.

Options:
  -v --verbose  Enable logging.
  -h --help
  --version
`

var Opts struct {
	Data    string `docopt:"DATAFILE"`
	Verbose bool   `docopt:"--verbose"`
}

func main() {

	// Handle SIGPIPE error
	c := make(chan os.Signal, 1)
	signal.Notify(c)

	// Options
	var optFlags docopt.Opts
	if optFlags, err = docopt.ParseArgs(usage, os.Args[1:], "0.0.1"); err != nil {
		log.Fatal(err)
	}
	optFlags.Bind(&Opts)

	// Logging
	log.SetOutput(os.Stderr)
	if Opts.Verbose {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.FatalLevel)
	}

	// Loads the Tensorflow model.
	start := time.Now()
	model = NewModel("models/hadr-clf-bilstm", "hadrClf")
	elapsed := time.Since(start)
	log.WithFields(log.Fields{
		"time": float64(elapsed.Seconds())}).Info("Loaded model.")

	// Infer
	start = time.Now()
	corpus := NewCorpus(Opts.Data, "data/stopwords.txt", "data/dic.json")
	elapsed = time.Since(start)
	log.WithFields(log.Fields{
		"time": float64(elapsed.Seconds())}).Info("Loaded corpus.")

	start = time.Now()
	var predictions []string
	if predictions, err = model.predict(corpus); err != nil {
		log.Fatal("Prediction failed on corpus.")
	}
	for _, p := range predictions {
		fmt.Println(p)
	}
	elapsed = time.Since(start)
	log.WithFields(log.Fields{
		"time": float64(elapsed.Seconds())}).Info("Completed inference.")
}
